package br.com.timedos2.controllers;

import br.com.timedos2.models.Campanha;
import br.com.timedos2.models.Cliente;
import br.com.timedos2.models.TimeDoCoracao;
import br.com.timedos2.service.ClienteService;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {

    @Mock
    private ClienteService service;
    @Mock
    private BindingResult validador;
    private ClienteController controller;

    @Before
    public void setUp() {

        controller = new ClienteController(service);

    }

    @Test
    public void testListarTodos() {

        when(service.listarTodos()).thenReturn(Arrays.asList(new Cliente()));

        ResponseEntity<List> result = controller.listarTodos();

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(1, result.getBody().size());

        verify(service, times(1)).listarTodos();
    }

    @Test
    public void testListarTodasDevolveNulo() {
        when(service.listarTodos()).thenReturn(null);

        ResponseEntity<List> result = controller.listarTodos();

        assertEquals(404, result.getStatusCodeValue());
        verify(service, times(1)).listarTodos();
    }

    @Test
    public void testBuscarPor() {
        when(service.buscarPor(any(Long.class))).thenReturn(new Cliente(1, "teste", null, null, null));

        ResponseEntity<Cliente> result = controller.buscarPor(1);

        Cliente body = result.getBody();
        assertEquals(200, result.getStatusCodeValue());
        assertEquals(1, body.getId());
        assertEquals("teste", body.getNomeCompleto());

        verify(service, times(1)).buscarPor(eq(1L));
    }

    @Test
    public void testBuscarPorDevolveErro() {
        when(service.buscarPor(any(Long.class))).thenReturn(null);

        ResponseEntity result = controller.buscarPor(1);

        assertEquals(404, result.getStatusCodeValue());
        verify(service, times(1)).buscarPor(eq(1L));
    }

    @Test
    public void testInserir() {
        Cliente cliente = new Cliente(1, "teste", null, null, null);
        when(service.cadastrar(any(Cliente.class))).thenReturn(cliente);

        ResponseEntity result = controller.inserir(new Cliente(), validador);

        assertEquals(200, result.getStatusCodeValue());
        verify(service, times(1)).cadastrar(any(Cliente.class));
    }
    @Test
    public void testInserirRepetido() {
        TimeDoCoracao timeDoCoracao = new  TimeDoCoracao();
        timeDoCoracao.setId(1);
        Cliente cliente = new Cliente(1, "teste", null, null, timeDoCoracao);
        Campanha campanha = new Campanha(10, "teste Campanha", LocalDate.MIN, LocalDate.MIN);
        campanha.setTimeId(1);
        cliente.setCampanhas(Arrays.asList(campanha));
        when(service.cadastrar(any(Cliente.class))).thenReturn(cliente);

        ResponseEntity<List<Campanha>> result = controller.inserir(cliente, validador);

        assertEquals(200, result.getStatusCodeValue());
        assertEquals(10, result.getBody().get(0).getId());
        assertEquals("teste Campanha", result.getBody().get(0).getNome());
        verify(service, times(1)).cadastrar(any(Cliente.class));
    }

    @Test
    public void testInserirDevolveErro() {
        when(service.cadastrar(any(Cliente.class))).thenReturn(null);

        ResponseEntity result = controller.inserir(new Cliente(), validador);

        assertEquals(404, result.getStatusCodeValue());
        verify(service, times(1)).cadastrar(any(Cliente.class));
    }

    @Test
    public void testInserirDevolveErroValidacao() {
        when(validador.hasErrors()).thenReturn(true);

        ResponseEntity result = controller.inserir(new Cliente(), validador);

        assertEquals(400, result.getStatusCodeValue());
        verify(service, times(0)).cadastrar(any(Cliente.class));
    }

    @Test
    public void testAlterar() {
        Cliente cliente = new Cliente(1, "teste", null, null, null);
        when(service.alterar(any(Long.class), any(Cliente.class))).thenReturn(cliente);

        ResponseEntity result = controller.alterar(1L, new Cliente(), validador);

        assertEquals(200, result.getStatusCodeValue());
        verify(service, times(1)).alterar(any(Long.class), any(Cliente.class));
    }

    @Test
    public void testAlterarDevolveErro() {
        when(service.alterar(any(Long.class), any(Cliente.class))).thenReturn(null);

        ResponseEntity result = controller.alterar(1L, new Cliente(), validador);

        assertEquals(404, result.getStatusCodeValue());
        verify(service, times(1)).alterar(any(Long.class), any(Cliente.class));
    }

    @Test
    public void testAlterarDevolveErroValidacao() {
        when(validador.hasErrors()).thenReturn(true);

        ResponseEntity result = controller.alterar(1L, new Cliente(), validador);

        assertEquals(400, result.getStatusCodeValue());
        verify(service, times(0)).alterar(any(Long.class), any(Cliente.class));
    }

    @Test
    public void testRemover() {
        Cliente cliente = new Cliente(1, "teste", null, null, null);
        when(service.remover(any(Long.class))).thenReturn(cliente);

        ResponseEntity result = controller.remover(1L);

        assertEquals(200, result.getStatusCodeValue());
        verify(service, times(1)).remover(any(Long.class));
    }

    @Test
    public void testRemoverDevolveErro() {
        when(service.remover(any(Long.class))).thenReturn(null);

        ResponseEntity result = controller.remover(1L);

        assertEquals(404, result.getStatusCodeValue());
        verify(service, times(1)).remover(any(Long.class));
    }

}
