package br.com.timedos2.service;

import br.com.timedos2.models.Cliente;
import br.com.timedos2.repositories.ClienteRepository;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.*;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServiceTest {

    @Mock
    private ClienteRepository dao;
    @Mock
    private RestTemplate restTemplate;
    private ClienteService service;

    @Before
    public void setUp() {
        service = new ClienteService(dao, restTemplate);
    }

    @Test
    public void testListarTodos() {
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        List<Cliente> clientes = Arrays.asList(new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null));
        when(dao.findAll()).thenReturn(clientes);

        List<Cliente> listarTodos = service.listarTodos();
        Cliente cliente = listarTodos.get(0);

        assertEquals(1, cliente.getId());
        assertEquals("teste", cliente.getNomeCompleto());
        assertEquals("teste@teste", cliente.getEmail());
        assertEquals(LocalDate.of(2010, 2, 1), cliente.getDataDeNascimento());
        assertNull(cliente.getTime());
        verify(dao, times(1)).findAll();
    }

    @Test
    public void testBuscarPor() {
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        Cliente cliente = new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null);
        when(dao.findOne(any(Long.class))).thenReturn(cliente);

        Cliente response = service.buscarPor(1);

        assertEquals(1, response.getId());
        assertEquals("teste", response.getNomeCompleto());
        assertEquals("teste@teste", response.getEmail());
        assertEquals(LocalDate.of(2010, 2, 1), response.getDataDeNascimento());
        assertNull(response.getTime());
        verify(dao, times(1)).findOne(any(Long.class));
    }

    @Test
    public void testAlterar() {
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        Cliente cliente = new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null);
        when(dao.findOne(any(Long.class))).thenReturn(cliente);
        when(dao.findByEmail(any(String.class))).thenReturn(null);
        when(dao.save(any(Cliente.class))).thenReturn(cliente);

        Cliente response = service.alterar(10, cliente);

        assertEquals(10, response.getId());
        assertEquals("teste", response.getNomeCompleto());
        assertEquals("teste@teste", response.getEmail());
        assertEquals(LocalDate.of(2010, 2, 1), response.getDataDeNascimento());
        assertNull(response.getTime());
        verify(dao, times(1)).findOne(any(Long.class));
    }

    @Test
    public void testCadastrar() {
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        Cliente cliente = new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null);
        when(dao.findByEmail(any(String.class))).thenReturn(null);
        when(dao.save(any(Cliente.class))).thenReturn(cliente);

        Cliente response = service.cadastrar(cliente);

        assertEquals(0, response.getId());
        assertEquals("teste", response.getNomeCompleto());
        assertEquals("teste@teste", response.getEmail());
        assertEquals(LocalDate.of(2010, 2, 1), response.getDataDeNascimento());
        assertNull(response.getTime());
        verify(dao, times(1)).findByEmail(any(String.class));
    }

    @Test
    public void testCadastrarEmailExistente() {
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        Cliente cliente = new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null);
        when(dao.findByEmail(any(String.class))).thenReturn(new Cliente());
        when(dao.save(any(Cliente.class))).thenReturn(cliente);

        Cliente response = service.cadastrar(cliente);

        assertEquals(0, response.getId());
        verify(dao, times(1)).findByEmail(any(String.class));
    }

    @Test
    public void testRemover() {
         when(restTemplate.exchange(any(String.class), any(HttpMethod.class), eq(null), any(ParameterizedTypeReference.class))).thenReturn(new ResponseEntity(HttpStatus.OK));
        Cliente cliente = new Cliente(1, "teste", "teste@teste", LocalDate.of(2010, 2, 1), null);
        when(dao.findOne(any(Long.class))).thenReturn(cliente);

        Cliente response = service.remover(1);

        assertEquals(1, response.getId());
        assertEquals("teste", response.getNomeCompleto());
        assertEquals("teste@teste", response.getEmail());
        assertEquals(LocalDate.of(2010, 2, 1), response.getDataDeNascimento());
        assertNull(response.getTime());
        verify(dao, times(1)).findOne(any(Long.class));
    }

}
