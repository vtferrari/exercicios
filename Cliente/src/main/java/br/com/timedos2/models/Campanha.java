package br.com.timedos2.models;

import java.io.Serializable;
import java.time.LocalDate;

public class Campanha implements Serializable {

    private long id;
    private String nome;
    private LocalDate inicioVigencia;
    private LocalDate fimVigencia;
    private long timeId;

    public Campanha() {
    }

    public Campanha(long id, String nome, LocalDate inicioVigencia, LocalDate fimVigencia) {
        this.id = id;
        this.nome = nome;
        this.inicioVigencia = inicioVigencia;
        this.fimVigencia = fimVigencia;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(LocalDate inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public LocalDate getFimVigencia() {
        return fimVigencia;
    }

    public void setFimVigencia(LocalDate fimVigencia) {
        this.fimVigencia = fimVigencia;
    }

    public long getTimeId() {
        return timeId;
    }

    public void setTimeId(long timeId) {
        this.timeId = timeId;
    }

}
