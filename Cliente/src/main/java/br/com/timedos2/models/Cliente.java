package br.com.timedos2.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import org.hibernate.validator.constraints.Email;

@Entity
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nomeCompleto;
    @Email
    private String email;
    private LocalDate dataDeNascimento;
    @Transient
    private List<Campanha> campanhas = new ArrayList<>();
    @ManyToOne(fetch = FetchType.EAGER)
    private TimeDoCoracao time;

    public Cliente() {
    }

    public Cliente(long id, String nomeCompleto, String email, LocalDate dataDeNascimento,TimeDoCoracao time) {
        this();
        this.id = id;
        this.nomeCompleto = nomeCompleto;
        this.email = email;
        this.dataDeNascimento = dataDeNascimento;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public List<Campanha> getCampanhas() {
        if (campanhas != null) {
            campanhas = campanhas.stream()
                    .filter(c -> time != null && c.getTimeId() == time.getId())
                    .collect(Collectors.toList());
        }
        return campanhas;
    }

    public void setCampanhas(List<Campanha> campanhas) {
        this.campanhas = campanhas;
    }

    public TimeDoCoracao getTime() {
        return time;
    }

    public void setTime(TimeDoCoracao time) {
        this.time = time;
    }

}
