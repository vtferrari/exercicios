package br.com.timedos2.controllers;

import br.com.timedos2.models.Cliente;
import br.com.timedos2.models.erro.Erro;
import br.com.timedos2.service.ClienteService;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ClienteController.class);
    private final ClienteService service;

    public ClienteController(ClienteService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity listarTodos() {
        List<Cliente> listarTodos = service.listarTodos();
        if (listarTodos == null || listarTodos.isEmpty()) {
            Erro erro = new Erro("Lista vazia.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(listarTodos, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity buscarPor(@PathVariable long id) {
        Cliente cliente = service.buscarPor(id);
        
        if (cliente == null) {
            Erro erro = new Erro("Campanha com id não foi encontrada.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(cliente, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity inserir(@Valid @RequestBody Cliente cliente, BindingResult validador) {
        if (validador.hasErrors()) {
            return new ResponseEntity(validador.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        Cliente cadastrar = service.cadastrar(cliente);
        if (cadastrar == null) {
            Erro erro = new Erro("Não é possivel cadastrar duas vezes", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }
        if (!cadastrar.getCampanhas().isEmpty()) {
            return new ResponseEntity(cadastrar.getCampanhas(), HttpStatus.OK);
        }
        return new ResponseEntity(cadastrar, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity alterar(@PathVariable long id, @Valid @RequestBody Cliente cliente, BindingResult validador) {
        if (validador.hasErrors()) {
            return new ResponseEntity(validador.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        Cliente alterar = service.alterar(id, cliente);
        if (alterar == null) {
            Erro erro = new Erro("Não é possivel alterar", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(alterar, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity remover(@PathVariable long id) {
        Cliente remover = service.remover(id);

        if (remover == null) {
            Erro erro = new Erro("Não é possivel remover", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(remover, HttpStatus.OK);
    }
}
