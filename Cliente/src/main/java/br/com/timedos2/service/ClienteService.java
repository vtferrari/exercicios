package br.com.timedos2.service;

import br.com.timedos2.models.Campanha;
import br.com.timedos2.models.Cliente;
import br.com.timedos2.repositories.ClienteRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
public class ClienteService {

    private final static Logger LOGGER = LoggerFactory.getLogger(ClienteService.class);
    private final ClienteRepository dao;
    private final RestTemplate restTemplate;

    @Autowired
    public ClienteService(ClienteRepository dao, RestTemplate restTemplate) {
        this.dao = dao;
        this.restTemplate = restTemplate;

    }

    @Cacheable("Clientes")
    public List<Cliente> listarTodos() {
        List<Cliente> todos = dao.findAll();
        todos.forEach((cliente) -> {
            buscaCampanhas(cliente);
        });
        return todos;
    }

    @Cacheable("Clientes")
    public Cliente buscarPor(long id) {
        return dao.findOne(id);
    }

    @CacheEvict(value = "Clientes", allEntries = true)
    public Cliente alterar(long id, Cliente cliente) {
        Cliente clienteDoBanco = dao.findOne(id);
        Cliente existe = dao.findByEmail(cliente.getEmail());
        if (clienteDoBanco != null) {
            if (existe == null || existe.getEmail().equals(clienteDoBanco.getEmail())) {
                cliente.setId(id);
                return dao.save(cliente);
            }
        }
        return null;
    }

    @CacheEvict(value = "Clientes", allEntries = true)
    public Cliente cadastrar(Cliente cliente) {
        Cliente existe = dao.findByEmail(cliente.getEmail());
        if (existe == null) {
            cliente.setId(0);
            return dao.save(cliente);
        }
        if (existe.getCampanhas().isEmpty()) {
            return buscaCampanhas(existe);
        }
        return null;
    }

    public Cliente remover(long id) {
        Cliente cliente = dao.findOne(id);
        if (cliente != null) {
            dao.delete(cliente);
            return cliente;
        }
        return null;

    }

    private Cliente buscaCampanhas(Cliente clienteComCampanha) {

        try {
            List<Campanha> campanhas = restTemplate
                    .exchange(
                            "http://localhost:5000/api/campanha",
                            HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<Campanha>>() {
                    }).getBody();
            clienteComCampanha.setCampanhas(campanhas);

            return clienteComCampanha;
        } catch (ResourceAccessException e) {
            LOGGER.error("[API-Campanha]->Servico campanha indisponivel", e.fillInStackTrace());
            return clienteComCampanha;
        }

    }
}
