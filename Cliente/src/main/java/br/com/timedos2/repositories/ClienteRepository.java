package br.com.timedos2.repositories;

import br.com.timedos2.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

    public Cliente findByEmail(String email);

}
