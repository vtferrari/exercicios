
# Console Application para encontrar Vogal .

Assim como os projetos anteriores foi utilizado o **Maven** para construção do projeto, porém, esse não requer nenhuma dependência a não ser as necessária para a execução dos alvos.

São dois os alvos para execução:

* **mvn exec:java** -> esse alvo executa a classe principal
* **mvn test** -> executa uma bateria de testes que foi usado para guiar o desenvolvimento;

```sh 
    # na pasta ./Stream
    $ mvn mvn exec:java
    # ... saidas
    $ mvn test
    # ... saidas
```     