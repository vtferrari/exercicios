package br.com.vinicius.stream;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CaractereStream implements Stream {

    private static final String PATTERN = "([aeiou][^aeiou][aeiou])";
    private final String vogaisUnicas;
    private final Matcher matcher;
    private char anterior;

    public CaractereStream(String input) {
        matcher = Pattern.compile(PATTERN, Pattern.CASE_INSENSITIVE).matcher(input);
        vogaisUnicas = getVogaisUnicas(input);
    }

    @Override
    public char getNext() {
        if (matcher.groupCount() > 0) {
            if (isUltimaVogal(matcher.group(1))) {
                anterior = matcher.group(1).charAt(2);
            }
            return anterior;
        }
        return 0;
    }

    @Override
    public boolean hasNext() {
        return matcher.find();
    }

    public static char firstChar(Stream input) {
        char next = 0;
        while (input.hasNext()) {
            next = input.getNext();
        }
        return next;
    }

    private boolean isUltimaVogal(String group) {
        String regex = ".*[" + vogaisUnicas.toLowerCase() + vogaisUnicas.toUpperCase() + "]";
        return !vogaisUnicas.isEmpty() && group.matches(regex);
    }

    private static String getVogaisUnicas(String input) {
        return Arrays.asList(input.split(""))
                .stream()
                .map(String::toUpperCase)
                .filter(c -> c.matches("[aeiouAEIOU]"))
                .sorted()
                .reduce("", (t, u) -> t.concat(u))
                .replaceAll("(\\w)\\1+", "");
    }

}
