package br.com.vinicius.stream;

public interface Stream {

    public char getNext();

    public boolean hasNext();

}
