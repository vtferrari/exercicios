package br.com.vinicius.stream;

import java.util.Arrays;

public class Executar {

    public static void main(String[] args) {
        String[] s = new String[]{
            "AaEeIiOoUu",
            "fFbBcCpPtTwW",
            "aaaaAAAAEEEEIIIeeeiiiOOOoooUUuuuuu",
            "AaEeIiOoUuFfWwWrR",
            "fFAabEeBIicoOCpUuPtTwW",
            "aAbBABacafe",
            "FaEbPiBIcfeOfU",
            "aAbBABicfeAfUu"
        };
        Arrays.asList(s)
                .stream()
                .forEach(i -> System.out.println(CaractereStream.firstChar(new CaractereStream(i))));

    }
}
