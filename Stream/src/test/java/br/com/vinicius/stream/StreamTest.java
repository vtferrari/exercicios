package br.com.vinicius.stream;

import org.junit.Test;
import static org.junit.Assert.*;

public class StreamTest {

    @Test
    public void testeDeveRetornarNadaQuandoNaoTemConsoante() {
        String input = "AaEeIiOoUu";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals(0, firstChar);
    }

    @Test
    public void testeDeveRetornarNadaQuandoNaoTemVogal() {
        String input = "fFbBcCpPtTwW";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals(0, firstChar);
    }

    @Test
    public void testeDeveRetornarNadaQuandoNaoTemVogalDepoisDeConsoante() {
       String input = "AaEeIiOoUuFfWwWrR";
       
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals(0, firstChar);
    }

    @Test
    public void testeDeveRetornarNadaQuandoTodasAsVogaisAparecem() {
        String input = "fFAabEeBIicoOCpUuPtTwW";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals(0, firstChar);
    }
    @Test
    public void testeDeveRetornarNadaQuandoTodasAsVogaisAparecemVariasVezes() {
        String input = "aaaaAAAAEEEEIIIeeeiiiOOOoooUUuuuuu";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals(0, firstChar);
    }

    @Test
    public void testeDeveAcharAVogalQuandoElaForOultimoCaracter() {
        String input = "aAbBABacafe";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals('e', firstChar);
    }

    @Test
    public void testeDeveAcharVogalRetonarQuandoElaNaoForUltimoCaracter() {
        String input = "aAbBABicfeAfUu";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));
        assertEquals('i', firstChar);
    }

    @Test
    public void testeDeveAcharvogalEetonarQuandoElaForSegundoCaracter() {
        String input = "TaEbBiBIcfeOfU";
        char firstChar = CaractereStream.firstChar(new CaractereStream(input));

        assertEquals('U', firstChar);
    }
}
