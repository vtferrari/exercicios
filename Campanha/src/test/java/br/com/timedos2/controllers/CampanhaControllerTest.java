package br.com.timedos2.controllers;

import br.com.timedos2.models.Campanha;
import br.com.timedos2.services.CampanhaService;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

@RunWith(MockitoJUnitRunner.class)
public class CampanhaControllerTest {

    @Mock
    private CampanhaService service;
    @Mock
    private BindingResult validador;
    private CampanhaController controller;

    @Before
    public void setUp() {

        controller = new CampanhaController(service);

    }

    @Test
    public void testListarTodas() {
        when(service.listarTodas(any(LocalDateTime.class))).thenReturn(Arrays.asList(new Campanha()));

        ResponseEntity<List> listarTodas = controller.listarTodas(null);

        assertEquals(200, listarTodas.getStatusCodeValue());
        assertEquals(1, listarTodas.getBody().size());

        verify(service, times(1)).listarTodas(any(LocalDateTime.class));
    }
    
    @Test
    public void testListarTodasComData() {
        when(service.listarTodas(any(LocalDateTime.class))).thenReturn(Arrays.asList(new Campanha()));

        ResponseEntity<List> listarTodas = controller.listarTodas("2017-01-01");

        assertEquals(200, listarTodas.getStatusCodeValue());
        assertEquals(1, listarTodas.getBody().size());

        verify(service, times(1)).listarTodas(any(LocalDateTime.class));
    }

    @Test
    public void testListarTodasDevolveNulo() {
        when(service.listarTodas(any(LocalDateTime.class))).thenReturn(null);

        ResponseEntity listarTodas = controller.listarTodas(null);

        assertEquals(404, listarTodas.getStatusCodeValue());
        verify(service, times(1)).listarTodas(any(LocalDateTime.class));
    }

    @Test
    public void testBuscarPor() {
        when(service.encontrarPor(any(Long.class))).thenReturn(new Campanha(1, "teste", null, null));

        ResponseEntity<Campanha> listarTodas = controller.buscarPor(1);

        Campanha body = listarTodas.getBody();
        assertEquals(200, listarTodas.getStatusCodeValue());
        assertEquals(1, body.getId());
        assertEquals("teste", body.getNome());

        verify(service, times(1)).encontrarPor(eq(1L));
    }

    @Test
    public void testBuscarPorDevolveErro() {
        when(service.listarTodas(any(LocalDateTime.class))).thenReturn(null);

        ResponseEntity listarTodas = controller.buscarPor(1);

        assertEquals(404, listarTodas.getStatusCodeValue());
        verify(service, times(1)).encontrarPor(eq(1L));
    }

    @Test
    public void testInserir() {
        Campanha campanha = new Campanha(1, "teste", null, null);
        when(service.cadastrar(any(Campanha.class))).thenReturn(campanha);

        ResponseEntity listarTodas = controller.inserir(new Campanha(), validador);

        assertEquals(200, listarTodas.getStatusCodeValue());
        verify(service, times(1)).cadastrar(any(Campanha.class));
    }

    @Test
    public void testInserirDevolveErro() {
        when(service.cadastrar(any(Campanha.class))).thenReturn(null);

        ResponseEntity listarTodas = controller.inserir(new Campanha(), validador);

        assertEquals(404, listarTodas.getStatusCodeValue());
        verify(service, times(1)).cadastrar(any(Campanha.class));
    }

    @Test
    public void testInserirDevolveErroValidacao() {
        when(validador.hasErrors()).thenReturn(true);

        ResponseEntity listarTodas = controller.inserir(new Campanha(), validador);

        assertEquals(400, listarTodas.getStatusCodeValue());
        verify(service, times(0)).cadastrar(any(Campanha.class));
    }

    @Test
    public void testAlterar() {
        Campanha campanha = new Campanha(1, "teste", null, null);
        when(service.alterar(any(Long.class), any(Campanha.class))).thenReturn(campanha);

        ResponseEntity listarTodas = controller.alterar(1L, new Campanha(), validador);

        assertEquals(200, listarTodas.getStatusCodeValue());
        verify(service, times(1)).alterar(any(Long.class), any(Campanha.class));
    }

    @Test
    public void testAlterarDevolveErro() {
        when(service.alterar(any(Long.class), any(Campanha.class))).thenReturn(null);

        ResponseEntity listarTodas = controller.alterar(1L, new Campanha(), validador);

        assertEquals(404, listarTodas.getStatusCodeValue());
        verify(service, times(1)).alterar(any(Long.class), any(Campanha.class));
    }

    @Test
    public void testAlterarDevolveErroValidacao() {
        when(validador.hasErrors()).thenReturn(true);

        ResponseEntity listarTodas = controller.alterar(1L, new Campanha(), validador);

        assertEquals(400, listarTodas.getStatusCodeValue());
        verify(service, times(0)).alterar(any(Long.class), any(Campanha.class));
    }

    @Test
    public void testRemover() {
        Campanha campanha = new Campanha(1, "teste", null, null);
        when(service.remover(any(Long.class))).thenReturn(campanha);

        ResponseEntity listarTodas = controller.remover(1L);

        assertEquals(200, listarTodas.getStatusCodeValue());
        verify(service, times(1)).remover(any(Long.class));
    }

    @Test
    public void testRemoverDevolveErro() {
        when(service.remover(any(Long.class))).thenReturn(null);

        ResponseEntity listarTodas = controller.remover(1L);

        assertEquals(404, listarTodas.getStatusCodeValue());
        verify(service, times(1)).remover(any(Long.class));
    }
}
