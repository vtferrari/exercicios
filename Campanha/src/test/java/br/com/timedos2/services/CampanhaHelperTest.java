package br.com.timedos2.services;

import br.com.timedos2.models.Campanha;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CampanhaHelperTest {

    private CampanhaHelper helper = new CampanhaHelper();

    @Test
    public void testAvancaUmDia() {

        Campanha campanha1 = new Campanha(1, "Campanha 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 03));
        Campanha campanha2 = new Campanha(2, "Campanha 2", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 02));
        Campanha campanha3 = new Campanha(3, "Campanha 3", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 04));
        Campanha campanha4 = new Campanha(4, "Campanha 4", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 06));

        List<Campanha> campanhasAtivas = Arrays.asList(campanha2, campanha1, campanha3, campanha4);

        campanhasAtivas = helper.realocaCampanhas(campanhasAtivas, new Campanha(5, "teste 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 03)));

        assertEquals(5, campanhasAtivas.size());

        assertEquals("teste 1", campanhasAtivas.get(0).getNome());
        assertEquals(03, campanhasAtivas.get(0).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 2", campanhasAtivas.get(1).getNome());
        assertEquals(04, campanhasAtivas.get(1).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 1", campanhasAtivas.get(2).getNome());
        assertEquals(05, campanhasAtivas.get(2).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 3", campanhasAtivas.get(3).getNome());
        assertEquals(06, campanhasAtivas.get(3).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 4", campanhasAtivas.get(4).getNome());
        assertEquals(07, campanhasAtivas.get(4).getFimVigencia().getDayOfMonth());

    }

    @Test
    public void testAvancaUmDiaSimplesNoDiaCertoInicio() {

        Campanha campanha1 = new Campanha(1, "Campanha 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 03));
        Campanha campanha2 = new Campanha(2, "Campanha 2", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 04));

        List<Campanha> campanhasAtivas = Arrays.asList(campanha2, campanha1);

        campanhasAtivas = helper.realocaCampanhas(campanhasAtivas, new Campanha(5, "teste 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 03)));

        assertEquals(3, campanhasAtivas.size());

        assertEquals("teste 1", campanhasAtivas.get(0).getNome());
        assertEquals(03, campanhasAtivas.get(0).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 1", campanhasAtivas.get(1).getNome());
        assertEquals(04, campanhasAtivas.get(1).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 2", campanhasAtivas.get(2).getNome());
        assertEquals(05, campanhasAtivas.get(2).getFimVigencia().getDayOfMonth());

    }

    @Test
    public void testAvancaUmDiaSimplesNoDiaCertoFim() {

        Campanha campanha1 = new Campanha(1, "Campanha 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 03));
        Campanha campanha2 = new Campanha(2, "Campanha 2", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 04));

        List<Campanha> campanhasAtivas = Arrays.asList(campanha2, campanha1);

        campanhasAtivas = helper.realocaCampanhas(campanhasAtivas, new Campanha(5, "teste 1", LocalDate.of(2017, 10, 01), LocalDate.of(2017, 10, 06)));

        assertEquals(3, campanhasAtivas.size());

        assertEquals("Campanha 1", campanhasAtivas.get(0).getNome());
        assertEquals(04, campanhasAtivas.get(0).getFimVigencia().getDayOfMonth());

        assertEquals("Campanha 2", campanhasAtivas.get(1).getNome());
        assertEquals(05, campanhasAtivas.get(1).getFimVigencia().getDayOfMonth());

        assertEquals("teste 1", campanhasAtivas.get(2).getNome());
        assertEquals(06, campanhasAtivas.get(2).getFimVigencia().getDayOfMonth());
    }

}
