/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.timedos2.services;

import br.com.timedos2.models.Campanha;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import br.com.timedos2.repositories.CampanhaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CampanhaServiceTest {

    @Mock
    private CampanhaRepository dao;
    private CampanhaHelper helper = new CampanhaHelper();
    private CampanhaService service;

    @Before
    public void setUp() {

        service = new CampanhaService(dao, helper);

    }

    @Test
    public void testListarTodas() {
        List<Campanha> campanhas = Arrays.asList(new Campanha(1, "C1", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10)));
        when(dao.listarAtivos(any(LocalDateTime.class))).thenReturn(campanhas);

        List<Campanha> listarTodas = service.listarTodas(LocalDateTime.of(2017, 10, 10, 0, 0));

        Campanha campanha = listarTodas.get(0);
        assertEquals(1, campanha.getId());
        assertEquals("C1", campanha.getNome());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getFimVigencia());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getInicioVigencia());
        verify(dao, times(1)).listarAtivos(any(LocalDateTime.class));
    }

    @Test
    public void testEncontrarPor() {
        Campanha campanhas = new Campanha(1, "C1", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10));
        when(dao.findOne(any(Long.class))).thenReturn(campanhas);

        Campanha campanha = service.encontrarPor(1);

        assertEquals(1, campanha.getId());
        assertEquals("C1", campanha.getNome());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getFimVigencia());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getInicioVigencia());
        verify(dao, times(1)).findOne(1L);
    }

    @Test
    public void testInserir() {

        List<Campanha> campanhas = Arrays.asList(new Campanha(1, "C1", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10)));
        when(dao.listarAtivos(any(LocalDateTime.class))).thenReturn(campanhas);

        Campanha nova = new Campanha(15, "CNova", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10));
        Campanha campanha = service.cadastrar(nova);

        assertEquals(0, campanha.getId());
        assertEquals("CNova", campanha.getNome());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getFimVigencia());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getInicioVigencia());
        verify(dao, times(1)).save(any(List.class));
    }

    @Test
    public void testAlterar() {
        when(dao.findOne(any(Long.class))).thenReturn(new Campanha(80, "C1", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10)));

        Campanha nova = new Campanha(80, "CNova", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10));
        when(dao.save(any(Campanha.class))).thenReturn(nova);
        Campanha campanha = service.alterar(80, nova);

        assertEquals(80, campanha.getId());
        assertEquals("CNova", campanha.getNome());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getFimVigencia());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getInicioVigencia());
        verify(dao, times(1)).findOne(80L);
        verify(dao, times(1)).save(eq(campanha));
    }

    @Test
    public void testAlterarInvalido() {
        when(dao.findOne(any(Long.class))).thenReturn(null);

        Campanha campanha = service.alterar(80, new Campanha());

        assertNull(campanha);
        verify(dao, times(1)).findOne(80L);
        verify(dao, times(0)).save(eq(campanha));
    }

    @Test
    public void testDeletar() {
        when(dao.findOne(any(Long.class))).thenReturn(new Campanha(80, "C1", LocalDate.of(2017, 10, 10), LocalDate.of(2017, 10, 10)));

        Campanha campanha = service.remover(80);

        assertEquals(80, campanha.getId());
        assertEquals("C1", campanha.getNome());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getFimVigencia());
        assertEquals(LocalDate.of(2017, 10, 10), campanha.getInicioVigencia());
        verify(dao, times(1)).findOne(80L);
        verify(dao, times(0)).save(eq(campanha));
    }

    @Test
    public void testDeletarInvalido() {
        when(dao.findOne(any(Long.class))).thenReturn(null);

        Campanha campanha = service.remover(80);

        assertNull(campanha);
        verify(dao, times(1)).findOne(80L);
        verify(dao, times(0)).delete(eq(campanha));
    }

}
