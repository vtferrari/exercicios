package br.com.timedos2.controllers;

import br.com.timedos2.models.Campanha;
import br.com.timedos2.models.error.Erro;
import br.com.timedos2.services.CampanhaService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/campanha")
public class CampanhaController {

    private final static Logger LOGGER = LoggerFactory.getLogger(CampanhaController.class);

    @Autowired
    private final CampanhaService service;

    public CampanhaController(CampanhaService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity listarTodas(@RequestParam(value = "data", required = false) String data) {
        LocalDateTime dataBusca = parseData(data);

        List<Campanha> listarTodas = service.listarTodas(dataBusca);
        if (listarTodas== null || listarTodas.isEmpty()) {
            Erro erro = new Erro("Lista vazia.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(listarTodas, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity buscarPor(@PathVariable long id) {
        Campanha campanha = service.encontrarPor(id);

        if (campanha == null) {
            Erro erro = new Erro("Campanha com id não foi encontrada.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(campanha, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity inserir(@Valid @RequestBody Campanha camapanha, BindingResult validador) {
        if (validador.hasErrors()) {
            return new ResponseEntity(validador.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        Campanha cadastrar = service.cadastrar(camapanha);
        if (cadastrar == null) {
            Erro erro = new Erro("Não foi possivel inserir.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cadastrar, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity alterar(@PathVariable long id, @Valid @RequestBody Campanha camapanha, BindingResult validador) {
        if (validador.hasErrors()) {
            return new ResponseEntity(validador.getAllErrors(), HttpStatus.BAD_REQUEST);
        }

        Campanha alterar = service.alterar(id, camapanha);

        if (alterar == null) {
            Erro erro = new Erro("Não foi possivel atualizar. Campanha com foi encontrada.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(alterar, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity remover(@PathVariable long id) {
        Campanha deletar = service.remover(id);

        if (deletar == null) {
            Erro erro = new Erro("Não foi possivel remover. Campanha com foi encontrada.", HttpStatus.NOT_FOUND.value());
            LOGGER.error(erro.getMensagem(), erro.getStatus());
            return new ResponseEntity(erro, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private LocalDateTime parseData(String data) {
        LocalDateTime dataBusca = LocalDateTime.now();
        if (data != null) {
            DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            dataBusca = LocalDate.parse(data, ofPattern).atStartOfDay();
        }
        return dataBusca;
    }

}
