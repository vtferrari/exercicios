package br.com.timedos2.services;

import br.com.timedos2.models.Campanha;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CampanhaHelper {

    public List<Campanha> realocaCampanhas(List<Campanha> campanhasAtivas, Campanha novaCampanha) {
        List<Campanha> campanhas = adicionaUmDia(campanhasAtivas);
        campanhas = verificaDiasRepetidos(novaCampanha, campanhas);
        campanhas = ordenaLista(campanhas);
        return campanhas;
    }

    private List<Campanha> adicionaUmDia(List<Campanha> campanhasAtivas) {
        List<Campanha> campanhas = campanhasAtivas
                .stream()
                .sorted((s1, s2) -> s1.getFimVigencia().compareTo(s2.getFimVigencia()))
                .map(c -> new Campanha(c.getId(), c.getNome(), c.getInicioVigencia(), c.getFimVigencia().plusDays(1)))
                .collect(Collectors.toList());
        return campanhas;
    }

    private List<Campanha> verificaDiasRepetidos(Campanha novaCampanha, List<Campanha> campanhas) {
        Campanha proximaCampanha = novaCampanha;
        for (Campanha campanha : campanhas) {
            if (proximaCampanha.getFimVigencia().equals(campanha.getFimVigencia())) {
                campanha.setFimVigencia(campanha.getFimVigencia().plusDays(1));
                proximaCampanha = campanha;
            }
        }
        campanhas.add(novaCampanha);
        return campanhas;
    }

    private static List<Campanha> ordenaLista(List<Campanha> campanhas) {
        return campanhas
                .stream()
                .sorted((s1, s2) -> s1.getFimVigencia().compareTo(s2.getFimVigencia()))
                .collect(Collectors.toList());
    }

}
