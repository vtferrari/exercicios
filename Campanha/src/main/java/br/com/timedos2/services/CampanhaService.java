package br.com.timedos2.services;

import br.com.timedos2.models.Campanha;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.com.timedos2.repositories.CampanhaRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

@Service
public class CampanhaService {

    @Autowired
    private final CampanhaRepository campanhaDao;
    private final CampanhaHelper campanhaHelper;

    public CampanhaService(CampanhaRepository campanhaDao, CampanhaHelper campanhaHelper) {
        this.campanhaDao = campanhaDao;
        this.campanhaHelper = campanhaHelper;
    }

    @Cacheable("Campanhas")
    public List<Campanha> listarTodas(LocalDateTime data) {
        return campanhaDao.listarAtivos(data);
    }

    @Cacheable("Campanhas")
    public Campanha encontrarPor(long id) {
        return campanhaDao.findOne(id);
    }

    @CacheEvict(value = "Campanhas", allEntries = true)
    public Campanha alterar(long id, Campanha camapanha) {
        Campanha campanhaDoBanco = campanhaDao.findOne(id);
        if (campanhaDoBanco != null) {
            camapanha.setId(id);
            return campanhaDao.save(camapanha);
        }
        return null;
    }

    @CacheEvict(value = "Campanhas", allEntries = true)
    public Campanha cadastrar(Campanha camapanha) {

        camapanha.setId(0);
        List<Campanha> listarTodas = listarTodas(LocalDateTime.now());
        List<Campanha> realocaCampanhas = campanhaHelper.realocaCampanhas(listarTodas, camapanha);
        campanhaDao.save(realocaCampanhas);

        return camapanha;

    }

    @CacheEvict(value = "Campanhas", allEntries = true)
    public Campanha remover(long id) {
        Campanha cliente = campanhaDao.findOne(id);
        if (cliente != null) {
            campanhaDao.delete(cliente);
            return cliente;
        }
        return null;
    }

}
