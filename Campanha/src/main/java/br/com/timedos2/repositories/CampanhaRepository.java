package br.com.timedos2.repositories;

import br.com.timedos2.models.Campanha;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CampanhaRepository extends JpaRepository<Campanha, Long> {
    @Query("From Campanha c Where :data between inicio_vigencia and fim_vigencia")
    public List<Campanha> listarAtivos(@Param("data") LocalDateTime data);

}
