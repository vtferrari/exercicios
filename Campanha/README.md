# 1) Serviços de campanha e 2) Sócio Torcedor.
Para o **correto** funcionamento dos serviços é necessário que ambos estejam rodando.

O projeto foi implementado usando **Maven e JAVA-8**, que são pré requisitos para a execução.

Foi usado um banco de dados em memoria: o **H2** para simular a persistência de dados, e gerar uma carga inicial do banco.

Nos dois projetos existem 3 alvos principais:

* **mvn test** -> que executa os tests;
* **mvn site** -> que aciona um framework de avaliação de cobertura de código;
* **mvn spring-boot:run** -> que é o comando principal para a execução do serviço


Para a a execução dos **alvos Maven** deve se entrar na pasta do projeto que deseja executar os comandos:
### Testes
```sh 
    # ex.: na pasta ./Campanha
    $ mvn test
```
### Cobertura
O framework irá criar uma pasta site com um **index.html** no diretório target ```./Campanha/target/site/``` lá exibe algumas informações sobre o projeto e sobre porcentagem de cobertura.
```sh 
    # na pasta ./Cliente
    $ mvn site
    $ cd target/site
    $ ls
```
### Execução do projeto
O projeto **Campanha** esta respondendo na porta 5000 e pode ser acessado pelo link local ```localhost:5000```;

Já o projeto **Cliente** responde através da porta 5001 e pode ser acessado pelo link local ```localhost:5001```;

> é recomendado o uso de dois terminais

http://localhost:5000/api/campanha -> mostra campanhas
```sh 
    # na pasta ./Campanha
    $ mvn spring-boot:run
```     
http://localhost:5001/api/cliente -> mostra clientes
```sh
    # na pasta ./Cliente
    $ mvn spring-boot:run
```   

Rotas para acesso aos recursos:
#### Campanha
 | Method | Route                                   | Param:type | Descrição                                    |
 | ------ |-----------------------------------------|------------|------------------------------------------------|
 | GET    | http://localhost:5000/api/campanha| | Lista todas as campanhas vigentes |
 | GET    | http://localhost:5000/api/campanha/{id} | id:number | Lista somente uma as campanha específica |
 | POST   | http://localhost:5000/api/campanha | object:JSON | Salva um campanha no banco de dados|
 | PUT    | http://localhost:5000/api/campanha/{id} | id:number, object:JSON  | Altera um campanha no banco de dados  |
 | DELETE | http://localhost:5000/api/campanha/{id} | id:number  | Remove um campanha do banco de dados|

______________________
