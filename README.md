# Introdução
Todos os exercícios estão em um único repositório para facilitar a correção e avaliação;

No projeto https://bitbucket.org/vtferrari/exercicios.git encontram-se as pastas:


| Pasta        | Projeto       | Numero Exercicio  |
| -------------|:--------------:| ----------------:|
| Campanha     | Campanha       |    1             |
| Cliente      | Socio Torcedor |    2             |
| Stream       | Primeira Vogal |    3             |

# 1) Serviços de campanha e 2) Sócio Torcedor.
Para o **correto** funcionamento dos serviços é necessário que ambos estejam rodando.

O projeto foi implementado usando **Maven e JAVA-8**, que são pré requisitos para a execução.

Foi usado um banco de dados em memoria: o **H2** para simular a persistência de dados, e gerar uma carga inicial do banco.

Nos dois projetos existem 3 alvos principais:

* **mvn test** -> que executa os tests;
* **mvn site** -> que aciona um framework de avaliação de cobertura de código;
* **mvn spring-boot:run** -> que é o comando principal para a execução do serviço


Para a a execução dos **alvos Maven** deve se entrar na pasta do projeto que deseja executar os comandos:
### Testes
```sh 
    # ex.: na pasta ./Campanha
    $ mvn test
```
### Cobertura
O framework irá criar uma pasta site com um **index.html** no diretório target ```./Campanha/target/site/``` lá exibe algumas informações sobre o projeto e sobre porcentagem de cobertura.
```sh 
    # na pasta ./Cliente
    $ mvn site
    $ cd target/site
    $ ls
```
### Execução do projeto
O projeto **Campanha** esta respondendo na porta 5000 e pode ser acessado pelo link local ```localhost:5000```;

Já o projeto **Cliente** responde através da porta 5001 e pode ser acessado pelo link local ```localhost:5001```;

> é recomendado o uso de dois terminais

http://localhost:5000/api/campanha -> mostra campanhas
```sh 
    # na pasta ./Campanha
    $ mvn spring-boot:run
```     
http://localhost:5001/api/cliente -> mostra clientes
```sh
    # na pasta ./Cliente
    $ mvn spring-boot:run
```   

Rotas para acesso aos recursos:
#### Campanha
 | Method | Route                                   | Param:type | Descrição                                    |
 | ------ |-----------------------------------------|------------|------------------------------------------------|
 | GET    | http://localhost:5000/api/campanha| | Lista todas as campanhas vigentes |
 | GET    | http://localhost:5000/api/campanha/{id} | id:number | Lista somente uma as campanha específica |
 | POST   | http://localhost:5000/api/campanha | object:JSON | Salva um campanha no banco de dados|
 | PUT    | http://localhost:5000/api/campanha/{id} | id:number, object:JSON  | Altera um campanha no banco de dados  |
 | DELETE | http://localhost:5000/api/campanha/{id} | id:number  | Remove um campanha do banco de dados|

______________________

#### Cliente
 | Method | Route                                   | Param:type | Descrição                                    |
 | ------ |-----------------------------------------|------------|------------------------------------------------|
 | GET    | http://localhost:5001/api/cliente| | Lista todos os cliente |
 | GET    | http://localhost:5001/api/cliente/{id} | id:number | Lista somente um cliente |
 | POST   | http://localhost:5001/api/cliente | object:JSON | Salva um cliente no banco de dados|
 | PUT    | http://localhost:5001/api/cliente/{id} | id:number, object:JSON  | Altera um cliente no banco de dados  |
 | DELETE | http://localhost:5001/api/cliente/{id} | id:number  | Remove um cliente do banco de dados|

**Obs:** Caso o projeto **Campanha** não estejam rodando o serviço **Cliente** responderá mais lentamente as primeiras requisições porem continuará em funcionamento, mas sem informações de Campanhas.

-------------

# Console Application para encontrar Vogal .

Assim como os projetos anteriores foi utilizado o **Maven** para construção do projeto, porém, esse não requer nenhuma dependência a não ser as necessária para a execução dos alvos.

São dois os alvos para execução:

* **mvn exec:java** -> esse alvo executa a classe principal
* **mvn test** -> executa uma bateria de testes que foi usado para guiar o desenvolvimento;

```sh 
    # na pasta ./Stream
    $ mvn mvn exec:java
    # ... saidas
    $ mvn test
    # ... saidas
```     


# Resposta para perguntas Java. 
4 )	O que é Deadlock? Detalhe um pouco sobre o caso e como você poderia resolver isso.

>Os Deadlocks são objetos que são acessados concorrentemente (thread), onde ele fica aguardando o fim de uma execução eternamente, ou seja: uma thread fica esperando a resposta de alguma outra thread e a mesma nunca acontece;
>Ex:
```
    Thread-0 -> espera Thread-1 e  Thread-1 -> espera Thread-0;
    nenhuma nunca notifica a outra de seu término;
    para facilitar o entendimento acima:
    Dois namorados brigam e param de se falar até que um peça desculpa para o outro.
    Se um deles não pedir desculpa, o outro não pede desculpas também.
```
>Como evitar:
>Para resolver o problema, devemos sempre notificar as threads na mesma ordem em todo o sistema. 
Sempre que os blocos com threads tiverem Syncronized garantir que as thread dependa minimamente de outra thread para sua finalização. 
Utilizar recursos de timeout para que a execução termine após algum tempo.

5 )	Uma das grandes inclusões no Java 8 foi a API Stream. Com ela podemos fazer diversas operações de loop, filtros, maps, etc. Porém, existe uma variação bem interessante do Stream que é ParallelStreams. Descreva com suas palavras quando qual é a diferença entre os dois e quando devemos utilizar cada um deles. 
>A API Stream devolve coleções manipuladas sem modificar a referência original, com ela é possível encadear interações em listas e executar vários tipos de execuções de forma funcional. Ela executa suas manipulações de forma syncrona, passando de uma manipulação para outra em ordem. É interessante utilizar esse tipo de Stream quando é feito manipulação em objetos imutáveis, pois a ordem de execução é sempre a mesma.
>ParallelStreams tem os mesmos recursos da anterior, porém a execução é assyncrona, ou seja o Java trabalha em cada lista de forma concorrente, ou seja esse tipo de Stream é interessante quando estamos utilizando objetos imutáveis (ex: AtomicInteger, entre outros).
Ex:
```java 
   listaInteiros.stream().filter(i->i>10).distinct().mapToInt(i->i*20).sum()
   //            1           2             3              4             5
 ```
 ```java 
   listaInteiros.parallelStream().filter(i->i>10).distinct().mapToInt(i->i*20).sum()
   //                  1           ?                ?              ?             5
 ```
>A execução pelo ParallelStreams pode ou não melhorar a performance da operação, mas isso depende muito da quantidade de dados, tipo de hardware, tipo do objeto manipulado entre outras coisas.